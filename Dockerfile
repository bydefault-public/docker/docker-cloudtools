FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest

WORKDIR /

ENV CLOUD_SDK_VERSION 375.0.0
ENV PATH /google-cloud-sdk/bin:$PATH
# ENV CLOUDSDK_PYTHON python3

RUN apk update && apk upgrade && apk add --update --no-cache \
    # gcompat \
    ansible \
    bash \
    build-base \
    ca-certificates \
    curl \
    gcc \
    gfortran \
    git \
    gzip \
    libaio \
    libnsl \
    libc6-compat \
    openssh \
    openssh-client \
    py-crcmod \
    py-pip \
    python3 \
    unzip \
    #CLEAN
    && rm -rf /var/cache/apk/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    # PIPS
    && pip install --no-cache-dir \
    cryptography \
    google \
    google-cloud \
    protobuf \
    #GCLOUD SDK
    # && ln -s /lib64/ld-linux-x86-64.so.2 /lib/ld-linux-x86-64.so.2 \
    # && CLOUD_SDK_VERSION=`curl -s https://api.github.com/repos/GoogleCloudPlatform/cloud-sdk-docker/tags | grep '"name": "[0-9]' | head -1 | awk '{print $2}' | cut -d '"' -f 2` \
    && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    && tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    && rm google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    # && curl -sSL https://sdk.cloud.google.com | bash \
    && ln -s /lib /lib64 \
    && ln -s /google-cloud-sdk/bin/gcloud /usr/bin/gcloud \
    && chmod +x /usr/bin/gcloud \
    #SDK CONFIG
    && gcloud config set core/disable_usage_reporting true \
    && gcloud config set component_manager/disable_update_check true \
    && gcloud config set metrics/environment github_docker_image \
    #KUBECTL
    && curl -sLO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && mv ./kubectl /usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl \
    # Ansible
    #CHECK
    && terraform -v \
    && gcloud -v \
    && kubectl version --short --client \
    && ansible --version


VOLUME ["/root/.config"]

ENTRYPOINT ["/bin/sh","-c"]